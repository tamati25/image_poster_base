#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""
Moe poster : Posts one image from the dropbox folder on twitter

TODO:
"""

__author__ = "/u/tama_92"

import os
import dropbox
import tweepy
import urllib2
import random
import magic
from datetime import datetime
import sys
import re
import traceback
import HTMLParser
import ConfigParser

# force UTF-8
reload(sys)
sys.setdefaultencoding("utf-8")

def log(text):
  print '[', str(datetime.now()), ']', text

# Read configuration file
configParser = ConfigParser.RawConfigParser()
configFilePath = 'moe_poster.py.config'
configParser.read(configFilePath)

if configParser.get('general', 'CAN_POST') == 'false' or configParser.get('general', 'MAINTENANCE') == 'true':
  log('Posting disabled in configuration file')
  sys.exit()

# environment variables
folder = configParser.get('general', 'FOLDER')
dropbox_folder = configParser.get('dropbox', 'DB_FOLDER')
max_retries = int(configParser.get('dropbox', 'DB_MAX_RETRIES'))

# dropbox api keys
DB_AUTH_TOKEN = configParser.get('dropbox', 'DB_TOKEN')
DB_FILE_SIZE_LIMIT = 3 * 1024 * 1024

# twitter access token
API_KEY = configParser.get('twitter', 'TWITTER_API_KEY')
API_SECRET = configParser.get('twitter', 'TWITTER_API_SECRET')
CONSUMER_KEY = configParser.get('twitter', 'TWITTER_CONSUMER_KEY')
CONSUMER_SECRET = configParser.get('twitter', 'TWITTER_CONSUMER_SECRET')

cooloff = int(configParser.get('general', 'COOLOFF'))

random_index = 0

debug = False
if len(sys.argv) > 1 and sys.argv[1] == 'd':
  debug = True
if configParser.get('general', 'IS_DEBUG') == 'true':
  debug = True

def can_post(cooloff, api, status_to_post):
  if cooloff > 20:
    cooloff = 20
  log("Status to check : " + status_to_post)
  log("Checking " + str(cooloff) + " latest tweets posted")
  latest_tweets = api.user_timeline()
  for status in latest_tweets:
    text = re.sub(" http://t\.co.*", "", status.text)
    log(text)
    if cooloff == 0:
      break
    if text == status_to_post:
      log("Already posted in a recent tweet")
      return False
    cooloff = cooloff - 1
  log("Tweet not found in the latest tweets")
  return True

# Download a file based on its number
# return true if the file downloaded is valid
def download_file(n):
  log('Trying picture #' + str(n))

  result = {}
  result["status"] = False
  filepath = files["contents"][n]["path"]
  filename = os.path.basename(filepath)

  log('Remote filename : ' + filename + ' (' + filepath + ')')

  localfilename = filename.encode('utf-8', 'ignore')
  localfile = open(folder + localfilename, 'wb')

  log('Local filename :' + localfilename)

  with client.get_file(filepath) as f:
    localfile.write(f.read())
  localfile.close()

  filetype = magic.from_file(folder + filename)
  if 'PNG' in filetype or 'JPEG' in filetype or 'GIF' in filetype:
    statinfo = os.stat(folder + filename)
    filesize = statinfo.st_size
    if filesize < DB_FILE_SIZE_LIMIT:
      result["status"] = True
      result["filename"] = filename
      result["localfilename"] = localfilename
      result["index"] = n
  return result

try:
  if debug is True:
    log('***** DEBUG MODE *****')
  else:
    log('***** PRODUCTION MODE *****')

  log('------')
  log('Starting')
  log('------')

  log('Connecting to dropbox')
  client = dropbox.client.DropboxClient(DB_AUTH_TOKEN)

  is_found = False
  log('Listing files')
  files = client.metadata(dropbox_folder)
  n_of_files = len(files["contents"])
  log(str(n_of_files) + " files found")

  localfilename = ''
  n_of_tries = 0
  random_index = 0
  result = {}
  ok = False

  while n_of_tries < max_retries and ok is False:
    if (debug is False and len(sys.argv) < 1) or (len(sys.argv) < 2):
      n_of_tries = n_of_tries + 1
      random_index = random.randint(1,n_of_files) - 1
      result = download_file(random_index)
      is_found = result["status"]
      if is_found is False:
        continue
    elif debug is False:
      result = download_file(int(sys.argv[1]))
    else:
      result = download_file(int(sys.argv[2]))

    # Decapsulate result from download_file method
    filename = result["filename"]
    localfilename = result["localfilename"].encode('utf-8', 'ignore')
    random_index = result["index"]

    #found one, print file name
    log('Posting "' + localfilename + '" to twitter')

    log('Connecting to twitter')
    auth = tweepy.OAuthHandler(API_KEY, API_SECRET)
    auth.set_access_token(CONSUMER_KEY, CONSUMER_SECRET)
    api = tweepy.API(auth)

    log('Posting...')
    localFilenameWithoutExt = ('').join(localfilename.split(".")[:-1])
    twitter_status = configParser.get('twitter', 'TWITTER_MESSAGE')
    twitter_status = twitter_status.replace('{#index}', str(random_index))
    twitter_status = twitter_status.replace('{#filename}', localFilenameWithoutExt)

    if can_post(cooloff, api, twitter_status) is False:
      continue

    # Do some processing on the text (TODO : put in a separate function)

    # truncate to 100 characters max
    if len(twitter_status) > 97:
      twitter_status = twitter_status[:97] + "..."

    # replace with the dictionary defined in "replace.txt" file
    replace_file_location = folder + "replace.txt"
    with open(replace_file_location.encode('utf-8')) as f:
      for line in f:
        if not line.startswith("#"):
          split = line.split()
          word_to_replace = ' '.join(split[:-1])
          replacement = split[-1:]

          if len(replacement) > 0:
            pattern = re.compile(re.escape(word_to_replace), re.IGNORECASE)
            twitter_status = re.sub(pattern, replacement[0], twitter_status)

            #Fix the consecutive # (FIXME : find a better way...)
            pattern = re.compile("#{2,}")
            twitter_status = re.sub(pattern, "#", twitter_status)

    h = HTMLParser.HTMLParser()
    status = HTMLParser.HTMLParser().unescape(twitter_status)

    if debug is False:
      api.update_with_media(folder + localfilename, status)
      log('Posted : ' + status + ' with picture ' + folder + localfilename)
    else:
      log('[DEBUG MODE] Status : ' + status + ', file path : ' + folder + localfilename)
      log('[DEBUG MODE] not posting.')

    log('Cleaning...')
    os.remove(folder + localfilename)
    ok = True
except Exception, e:
  tb = traceback.format_exc()
  log('Error : ' + tb)

if ok is False:
  log("Max retries reached")

log('------')
log('Finished')
log('------')
