#!/bin/bash

minute=$(date +%M)
if [[ $(($minute % 30)) -eq 0 ]]; then
    cd ${OPENSHIFT_REPO_DIR}
    python moe_poster.py > "${OPENSHIFT_REPO_DIR}/log.txt"
fi
echo 'Test' > "${OPENSHIFT_REPO_DIR}/test.txt"
